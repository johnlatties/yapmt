using System;
using YAPMT.Domain.Enums;

namespace YAPMT.Application.Services.Mapper.Dtos.Project
{
    public class TaskProjectDto
    {
        public Guid Id { get;  set; }
        public Guid ProjectId { get;  set; }
        public string Description { get;  set; }
        public string Owner { get;  set; }
        public DateTime DueDate { get;  set; }
        public TaskStatus Status { get;  set; }

        public bool Completed
        {
            get
             {
                return Status == TaskStatus.Completed;
            }
        }
    }
}