using Microsoft.AspNetCore.Mvc;

namespace YAPMT.Application.Services.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index() => Redirect("~/swagger");
    }
}