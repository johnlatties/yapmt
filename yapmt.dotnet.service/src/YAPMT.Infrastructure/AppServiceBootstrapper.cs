

using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using YAPMT.Core.Domain.Repositories;
using YAPMT.Core.Infrastructure.Repositories.Document;
using YAPMT.Core.Infrastructure.Services;

namespace YAPMT.Infrastructure
{
    public static class AppServiceBootstrapper
    {
        public static void ConfigureService(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(new MongoUrl(configuration.GetConnectionString("AppDocumentConnection")));
            services.AddSingleton<IMongoClient>(provider =>
            {
                var mongoUrl = provider.GetService<MongoUrl>();
                return new MongoClient(mongoUrl);
            });

            services.AddScoped(typeof(IWriteOnlyRepository<>), typeof(WriteOnlyDocumentRepository<>));
            services.AddScoped(typeof(IReadOnlyRepository<>), typeof(ReadOnlyDocumentRepository<>));

            services.Scan(scan =>
            {
                scan.FromApplicationDependencies()
                    .AddClasses(filter => filter.AssignableTo<IRepository>())
                        .AsImplementedInterfaces()
                        .WithScopedLifetime()

                    .AddClasses(FilterDefinition => FilterDefinition.AssignableTo<IService>())
                        .AsImplementedInterfaces()
                        .AsSelf()
                        .WithScopedLifetime();

            });

            services.AddMediatR();
            

        }
    }
}