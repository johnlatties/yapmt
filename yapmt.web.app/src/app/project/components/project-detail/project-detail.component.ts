import { Component, OnInit } from '@angular/core';
import { ProjectServer } from '../../services/project.server';
import { SnackbarServiceService } from '../../services/snackbar-service.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MessageBusService } from '../../services/message-bus.service';
import { CREATE_PROJECT, GET_PROJECT, GET_PROJECTS } from '../../services/actionsType';
import { Subscription } from 'rxjs/Subscription';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { TaskStatus } from '../../services/task-status.enum';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css']
})
export class ProjectDetailComponent implements OnInit, OnDestroy {

  form: FormGroup;
  projectName;
  project;
  isReadonly: boolean;
  showForm: boolean;
  statusProject = '';
  createProjectSubcription: Subscription;
  getCreateSubcription: Subscription;
  constructor(
    public snackbar: SnackbarServiceService,
    private projectServer: ProjectServer,
    public messageBusService: MessageBusService
  ) {

    this.createProjectSubcription = this.messageBusService
      .of(CREATE_PROJECT)
      .subscribe((showForm) => {
        this.isReadonly = false;
        this.project = null;
        this.projectName = '';
        this.showForm = showForm;
      });

    this.getCreateSubcription = this.messageBusService
      .of(GET_PROJECT)
      .subscribe((projectId) => {
        this.showForm = true;
        this.loadProject(projectId);
      });
  }

  ngOnInit() {
    this.form = new FormGroup({
      projectName: new FormControl('', [
        Validators.required,
      ])
    });
  }

  onSave(e) {
    e.preventDefault();
    if (this.isReadonly) { return; }
    if (this.form.invalid) {
      this.snackbar.showWarning('Project name is required.');
      return;
    }
    this.projectServer.createProject({ name: this.projectName })
      .subscribe((data) => {
        this.snackbar.showSuccessDefault();
        this.isReadonly = true;
        if (data.hasOutputData) {
          this.loadProject(data.outputData.id);
          this.messageBusService.publish(GET_PROJECTS, '');
        }
      }, () => this.notifyErro());
  }

  onDelete() {
    this.projectServer.deleteProject(this.project.id)
      .subscribe(() => {
        this.snackbar.showSuccessDefault();
        this.messageBusService.publish(GET_PROJECTS, '');
      }, () => this.snackbar.showWarningDefault());
  }

  notifyErro() {
    this.isReadonly = false;
    this.snackbar.showWarningDefault();
  }

  loadProject(id) {
    this.project = null;
    this.projectServer.getProject(id)
      .subscribe((data) => {
        this.isReadonly = true;
        this.project = data;
        this.projectName = this.project.name;
        this.showStatusProject();
      }, () => this.notifyErro());
  }

  showStatusProject() {
    this.statusProject = '';
    const completed = this.onGetTotalTypeTask(TaskStatus.Completed);
    const late = this.onGetTotalTypeTask(TaskStatus.Late);
    this.statusProject = `${completed}/${late}/${this.project.tasks.length}`;
  }

  onGetTotalTypeTask(type) {
    return this.project.tasks.filter((t) => t.status === type).length;
  }


  ngOnDestroy(): void {
    this.createProjectSubcription.unsubscribe();
    this.getCreateSubcription.unsubscribe();
  }

}
