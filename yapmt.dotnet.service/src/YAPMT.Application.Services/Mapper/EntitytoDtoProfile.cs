using AutoMapper;
using YAPMT.Application.Services.Mapper.Dtos.Project;
using YAPMT.Domain.Projects.Entities;

namespace YAPMT.Infrastructure.Mapper
{
    public class EntitytoDtoProfile : Profile
    {
        public EntitytoDtoProfile()
        {
            CreateMap<Project, ProjectDto>();
            CreateMap<TaskProject, TaskProjectDto>();
            CreateMap<Project, ProjectWithTaksDto>();
        }
    }
}