﻿using System;
using System.Threading;
using System.Threading.Tasks;
using YAPMT.Core.Domain.Handlers;
using YAPMT.Domain.Projects.Commands;
using YAPMT.Domain.Projects.Entities;
using YAPMT.Domain.Projects.Filter;
using YAPMT.Domain.Projects.Repositories;

namespace YAPMT.Domain.Projects.CommandHandlers
{
    public class CreateTaskProjectCommandHandler : IHandlerAsync<CreateTaskProjectCommand>
    {
        private readonly IProjectWriteOnlyDocumentRepository ProjectWriteRepositoty;
        private readonly IProjectReadOnlyDocumentRepository ProjectReadRepositoty;
        public CreateTaskProjectCommandHandler(
            IProjectWriteOnlyDocumentRepository projectWriteRepositoty,
            IProjectReadOnlyDocumentRepository projectReadRepositoty)
        {
            ProjectWriteRepositoty = projectWriteRepositoty;
            ProjectReadRepositoty = projectReadRepositoty;
        }
        public async Task<Notification> Handle(CreateTaskProjectCommand request, CancellationToken cancellationToken)
        {
            var project = await ProjectReadRepositoty.GetAsync(ProjectFilter.ById(request.ProjectId));

            if(project is null)
            {
                var notification = new Notification();
                notification.AddValidation("PojectNotDeleted", $"Could not find the project : {request.ProjectId}");
                return notification;
            }
             else
            {

                var notificationError = Validate(request);
                if (notificationError.HasMessages)
                    return notificationError;
                
                var task = new TaskProject(request.ProjectId, request.Description, request.Owner, request.DueDate);
                project.AddTask(task);
                await ProjectWriteRepositoty.InsertTaskProjectAsync(task);
                
                return new Notification(task);
            }
        }


        private Notification Validate(CreateTaskProjectCommand commad)
        {
            Notification notification = new Notification();
            if (string.IsNullOrEmpty(commad.Description))
            {
                notification.AddValidation("CreateTaskProject", "The task description is required.");
                return notification;
            }
            if (string.IsNullOrEmpty(commad.Owner))
            {
                notification.AddValidation("CreateProject", "The task owner is required.");
                return notification;
            }
            if (commad.DueDate == default(DateTime))
            {
                notification.AddValidation("CreateProject", "The due due date is required.");
                return notification;
            }
            return notification;
        }
    }
}
