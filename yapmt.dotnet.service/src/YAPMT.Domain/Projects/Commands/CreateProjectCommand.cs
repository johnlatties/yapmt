using YAPMT.Core.Domain.Commands;

namespace YAPMT.Domain.Projects.Commands
{
    public class CreateProjectCommand : ICommand
    {
        public string Name { get; set; }
    }
}