using System.Threading.Tasks;
using MediatR;
using YAPMT.Core.Domain.Commands;
using YAPMT.Core.Domain.Handlers;
using YAPMT.Core.Infrastructure.Services;

namespace YAPMT.Core.Infrastructure.Bus
{
    public class MediatorService : IMediatorService
    {
        private readonly IMediator mediator;
        public MediatorService(IMediator mediatR)
        {
            mediator = mediatR;
        }

        public Task RaiseEventAsync(INotificationHandler @event)
        {
            return mediator.Publish(@event);
        }

        public Task<Notification> SendCommandAsync(ICommand command)
        {
            return mediator.Send(command);
        }
    }
}