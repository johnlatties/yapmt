namespace YAPMT.Core.Domain.Handlers
{
    public class Notification : NotificationBase<object>
    {
        public Notification()
        {
        }
        public Notification(object outputData) : base(outputData)
        {
        }
        public virtual TData GetOutputData<TData>() => (TData)base.GetOutputData();
    }
}