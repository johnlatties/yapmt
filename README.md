# README #

##YAPMT 

### .net core 2.0 servie ###
# 
* \yapmt.dotnet.service
#Settings:
* Set ConnectionStrings key AppDocumentConnection to MongoDB.
* Ex.:  "AppDocumentConnection" : "mongodb://localhost:27017/yapmt".
* The startup projects is \yapmt.dotnet.service\src\YAPMT.Application.Services.
* running the application in your local environment, you can access http://localhost:5000/swagger/ to see the api documentation.

### Web application ###
#
* \yapmt.web.app
#Settings:
* run `npm install` to install to install the necessary packages 
* And run `npm server` or `npm start` to start the app. 
* In the \yapmt.web.app\src\app\app.config.ts file is the access link for the backend api service
* Ex.: export const API_ROOT = 'http://localhost:5000/api';
