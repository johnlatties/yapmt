﻿using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using YAPMT.Core.Infrastructure.Repositories.Document;
using YAPMT.Domain.Projects.Entities;
using YAPMT.Domain.Projects.Repositories;

namespace YAPMT.Infrastructure.Repositories.Document
{
    public class ProjectWriteOnlyDocumentRepository : WriteOnlyDocumentRepository<Project>, IProjectWriteOnlyDocumentRepository
    {
        public ProjectWriteOnlyDocumentRepository(IMongoClient mongoClient, MongoUrl mongoUrl)
        : base(mongoClient, mongoUrl)
        {
        }
        public async Task InsertTaskProjectAsync(TaskProject task)
        {
            ValidateEntityArgument(task);

            await Collection
            .FindOneAndUpdateAsync(p => p.Id == task.ProjectId, Builders<Project>.Update.Push(p => p.Tasks, task));
        }


        public async Task UpdateTaskProjectAsync(TaskProject task)
        {
            ValidateEntityArgument(task);
            var update = Builders<Project>.Update.Set("Tasks.$.Status", task.Status);
            await Collection.UpdateOneAsync(
                p => p.Id == task.ProjectId && p.Tasks.Any(t => t.Id == task.Id),
                update,
                new UpdateOptions() { IsUpsert = true }); 
        }
    }
}
