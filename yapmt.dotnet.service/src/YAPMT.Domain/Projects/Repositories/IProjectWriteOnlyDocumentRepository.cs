﻿using System.Threading.Tasks;
using YAPMT.Core.Domain.Repositories;
using YAPMT.Domain.Projects.Entities;

namespace YAPMT.Domain.Projects.Repositories
{
    public interface IProjectWriteOnlyDocumentRepository : IWriteOnlyRepository<Project>
    {
        Task InsertTaskProjectAsync(TaskProject task);

        Task UpdateTaskProjectAsync(TaskProject task);
    }
}
