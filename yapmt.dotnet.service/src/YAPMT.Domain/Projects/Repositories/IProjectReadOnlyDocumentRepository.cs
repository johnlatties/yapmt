using System.Threading.Tasks;
using YAPMT.Core.Domain.Repositories;
using YAPMT.Core.Domain.Specifications;
using YAPMT.Domain.Projects.Entities;

namespace YAPMT.Domain.Projects.Repositories
{
    public interface IProjectReadOnlyDocumentRepository :  IReadOnlyRepository<Project>
    {
         Task<Project> GetTaskProject(IQueryFilter<Project> spec);
    }
}