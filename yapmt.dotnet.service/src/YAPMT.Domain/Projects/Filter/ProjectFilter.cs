﻿using System;
using System.Linq;
using YAPMT.Core.Domain.Specifications;
using YAPMT.Domain.Enums;
using YAPMT.Domain.Projects.Entities;

namespace YAPMT.Domain.Projects.Filter
{
    public static class ProjectFilter
    {
        public static QueryFilter<Project> ById(Guid id)
        {
            return new DirectFilter<Project>(p => p.Id == id);
        }

        public static QueryFilter<Project> ByIdWithLateTaskToUpdate(Guid id)
        {
            return new DirectFilter<Project>(p => p.Id == id 
            && p.Tasks.Any(t => t.DueDate < DateTime.Now && t.Status == TaskStatus.InTime));
        }
    }
}
