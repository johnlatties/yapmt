using System.Collections.Generic;

namespace YAPMT.Application.Services.Mapper.Dtos.Project
{
    public class ProjectWithTaksDto : ProjectDto
    {
        public ICollection<TaskProjectDto> Tasks { get; set; }
        
    }
}