using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using YAPMT.Core.Domain.Handlers;
using YAPMT.Domain.Projects.Commands;
using YAPMT.Domain.Projects.Filter;
using YAPMT.Domain.Projects.Repositories;

namespace YAPMT.Domain.Projects.CommandHandlers
{
    public class UpdateTaskProjectToCompletedCommandHandler : IHandlerAsync<UpdateStatusTaskProjectCommand>
    {
        private readonly IProjectWriteOnlyDocumentRepository ProjectWriteRepositoty;
        private readonly IProjectReadOnlyDocumentRepository ProjectReadRepositoty;

        public UpdateTaskProjectToCompletedCommandHandler(
             IProjectWriteOnlyDocumentRepository projectWriteRepositoty,
            IProjectReadOnlyDocumentRepository projectReadRepositoty
        )
        {
            ProjectWriteRepositoty = projectWriteRepositoty;
            ProjectReadRepositoty = projectReadRepositoty;
        }

        public async Task<Notification> Handle(
            UpdateStatusTaskProjectCommand request
            , CancellationToken cancellationToken)
        {
            var project = await ProjectReadRepositoty.GetAsync(ProjectFilter.ById(request.ProjectId));

            if (project is null)
            {
                var notification = new Notification();
                notification.AddValidation("PojectNotDeleted", $"Could not find the project : {request.ProjectId}");
                return notification;
            }

            var task = project.Tasks?.FirstOrDefault(t => t.Id == request.TaskId);
            if (task is null)
            {
                var notification = new Notification();
                notification.AddValidation("PojectNotDeleted", $"Could not find the task : {request.TaskId}");
                return notification;
            }

            task.SetStatus(request.Status);

            await ProjectWriteRepositoty.UpdateTaskProjectAsync(task);

            return new Notification(task);

        }
    }
}