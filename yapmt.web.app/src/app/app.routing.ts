import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectComponent } from './project/project.component';
import { ProjectViewComponent } from './project/components/project-view/project-view.component';
import { AppComponent } from './app.component';


export const routes: Routes = [
    {
        path: '', component: AppComponent,
        // children:
        //     [
        //         { path: 'project', component: ProjectViewComponent }
        //     ],
    },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true });
