import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
    name: 'addAtSignal',
})
export class AddAtSignalPipe implements PipeTransform {
    transform(value: string): string {
        return `@${value}`;
    }
}
