import { Component, OnInit, Input } from '@angular/core';
import { AddAtSignalPipe } from '../../pipe/add-at-signal.pipe';
import { SnackbarServiceService } from '../../services/snackbar-service.service';
import { ProjectServer } from '../../services/project.server';
import { MessageBusService } from '../../services/message-bus.service';
import { GET_PROJECT } from '../../services/actionsType';
import { TaskStatus } from '../../services/task-status.enum';
import { RelativeDatePipe } from '../../pipe/relative-date.pipe';


@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

  @Input() tasks: any[] = [];
  constructor(
    public snackbar: SnackbarServiceService,
    private projectServer: ProjectServer,
    public messageBusService: MessageBusService
  ) { }

  ngOnInit() {
  }

  onGetDescription(task) {
    const owner = AddAtSignalPipe.prototype.transform(task.owner);
    const time = RelativeDatePipe.prototype.transform(task.dueDate);
    return `${task.description}, ${owner}, ${time}`;
  }

  onSetClass(task) {
    switch (task.status) {
      case TaskStatus.Completed:
        return 'task-completed';
      case TaskStatus.Late:
        return 'task-late';
      default:
        return '';
    }
  }

  onUpdateStatus(task) {
    const newStatus = !task.completed ? TaskStatus.Completed
      : (new Date(task.dueDate) > new Date())
        ? TaskStatus.InTime : TaskStatus.Late;

    this.projectServer
      .updateTask(task.projectId, task.id, newStatus)
      .subscribe(() => {
        this.snackbar.showSuccessDefault();
        this.messageBusService.publish(GET_PROJECT, task.projectId);
      }, () => this.snackbar.showWarningDefault());
  }

}
