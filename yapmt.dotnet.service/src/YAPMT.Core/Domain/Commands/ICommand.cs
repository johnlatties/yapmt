using MediatR;
using YAPMT.Core.Domain.Handlers;

namespace YAPMT.Core.Domain.Commands
{
    public interface ICommand : IRequest<Notification>
    {
    }
}