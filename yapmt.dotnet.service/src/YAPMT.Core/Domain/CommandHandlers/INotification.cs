namespace YAPMT.Core.Domain.Handlers
{
    public interface INotificationHandler : MediatR.INotification
    {
    }
}