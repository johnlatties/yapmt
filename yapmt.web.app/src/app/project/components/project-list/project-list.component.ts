import { Component, OnInit } from '@angular/core';
import { ProjectServer } from '../../services/project.server';
import { MessageBusService } from '../../services/message-bus.service';
import { CREATE_PROJECT, GET_PROJECT, GET_PROJECTS } from '../../services/actionsType';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit, OnDestroy {

  private projects: any[] = [];
  getAllSubcription: Subscription;
  selectedId;
  constructor(
    private projectServer: ProjectServer,
    public messageBusService: MessageBusService
  ) {

    this.getAllSubcription = this.messageBusService
      .of(GET_PROJECTS)
      .subscribe(() => this.fetchProjects());
  }

  ngOnInit() {
    this.fetchProjects();
  }

  fetchProjects() {
    this.projectServer.getProjects()
      .subscribe((result) => {
        this.projects = result || [];
        if (this.projects.length > 0) {
          this.projects = this.projects.reverse();
          this.notifyProject(this.projects[0].id);
        } else {
          this.messageBusService.publish(CREATE_PROJECT, false);
        }
      });
  }

  notifyProject(projectId) {
    this.selectedId = projectId;
    this.messageBusService.publish(GET_PROJECT, projectId);
  }

  onSelect(id) {
    this.notifyProject(id);
  }

  ngOnDestroy(): void {
    this.getAllSubcription.unsubscribe();
  }

}
