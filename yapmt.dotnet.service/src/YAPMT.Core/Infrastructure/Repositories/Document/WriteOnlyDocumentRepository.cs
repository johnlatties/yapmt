using MongoDB.Driver;
using System.Threading.Tasks;
using YAPMT.Core.Domain.Entities;
using YAPMT.Core.Domain.Repositories;

namespace YAPMT.Core.Infrastructure.Repositories.Document
{
    public class WriteOnlyDocumentRepository<TEntity> :  DocumentRepository<TEntity>,  IWriteOnlyRepository<TEntity>
     where TEntity : class, IEntity
    {
        public WriteOnlyDocumentRepository(IMongoClient mongoClient, MongoUrl mongoUrl)
        :base(mongoClient, mongoUrl)
        {
        }

        public Task InsertAsync(TEntity entity)
        {
            ValidateEntityArgument(entity);
            return Collection.InsertOneAsync(entity);
        }

        public Task UpdateAsync(TEntity entity)
        {
            ValidateEntityArgument(entity);
            return Collection.ReplaceOneAsync(filterEntity => filterEntity.Id.Equals(entity.Id), entity);
        }
        public Task DeleteAsync(TEntity entity)
        {
             ValidateEntityArgument(entity);
            return Collection.DeleteOneAsync(filterEntity => filterEntity.Id.Equals(entity.Id));
        }

    }
}