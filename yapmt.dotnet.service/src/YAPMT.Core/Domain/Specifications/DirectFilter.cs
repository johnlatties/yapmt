using System;
using System.Linq.Expressions;
using YAPMT.Core.Domain.Entities;

namespace YAPMT.Core.Domain.Specifications
{
    public class DirectFilter<TEntity> : QueryFilter<TEntity> where TEntity : class, IEntity
    {
        private readonly Expression<Func<TEntity, bool>> Expression;
        public DirectFilter(Expression<Func<TEntity, bool>> expression)
        {
            Expression = expression ?? throw new ArgumentException(nameof(expression));
        }
        public override Expression<Func<TEntity, bool>> SatisfiedBy() => Expression;
    }
}