using System.Threading;
using System.Threading.Tasks;
using YAPMT.Core.Domain.Handlers;
using YAPMT.Core.Domain.Repositories;
using YAPMT.Domain.Projects.Commands;
using YAPMT.Domain.Projects.Entities;

namespace YAPMT.Domain.Projects.CommandHandlers
{
    public class CreateProjectCommandHnadler : IHandlerAsync<CreateProjectCommand>
    {
        private readonly IWriteOnlyRepository<Project> ProjectRepository;
        public CreateProjectCommandHnadler(IWriteOnlyRepository<Project> projectRepository)
        {
            ProjectRepository = projectRepository;
        }
        public async Task<Notification> Handle(CreateProjectCommand request, CancellationToken cancellationToken)
        {
            var notificationError = Validate(request);
            if (notificationError.HasMessages)
                return notificationError;

            var project = new Project(request.Name);

            await ProjectRepository.InsertAsync(project);

            return new Notification(project);
        }
        private Notification Validate(CreateProjectCommand commad)
        {
            Notification notification = new Notification();
            if (string.IsNullOrEmpty(commad.Name))
            {
                notification.AddValidation("CreateProject", "The task name is required.");
                return notification;
            }
            return notification;
        }
    }
}