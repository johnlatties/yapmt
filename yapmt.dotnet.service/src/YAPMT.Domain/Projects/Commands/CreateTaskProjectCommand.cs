﻿using System;
using YAPMT.Core.Domain.Commands;

namespace YAPMT.Domain.Projects.Commands
{
    public class CreateTaskProjectCommand : ICommand
    {
        public Guid ProjectId { get;  set; }
        public string Description { get;  set; }
        public string Owner { get;  set; }
        public DateTime DueDate { get;  set; }
    }
}
