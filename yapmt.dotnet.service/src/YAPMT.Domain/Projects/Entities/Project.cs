using System.Collections.Generic;
using System.Linq;
using YAPMT.Core.Domain.Entities;

namespace YAPMT.Domain.Projects.Entities
{
    public class Project : Entity
    {
        protected Project()
        {
            Tasks = new List<TaskProject>();
        }
        public Project(string name)
        {
            Name = name;
            Tasks = new List<TaskProject>();
        }


        public string Name { get; private set; }
        public ICollection<TaskProject> Tasks { get; private set; }

        public void AddTask(TaskProject newTask)
        {
            Tasks.Add(newTask);
        }
    }
}