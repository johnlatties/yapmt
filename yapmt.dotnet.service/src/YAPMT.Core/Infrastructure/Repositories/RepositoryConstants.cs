﻿namespace YAPMT.Core.Infrastructure.Repositories
{
    public static class RepositoryConstants
    {
        public const int DefaultQueryLimitCount = 100;
    }
}
