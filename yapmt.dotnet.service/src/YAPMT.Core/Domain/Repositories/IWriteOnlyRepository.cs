using System.Threading.Tasks;
using YAPMT.Core.Domain.Entities;

namespace YAPMT.Core.Domain.Repositories
{
    public interface IWriteOnlyRepository<TEntity> : IRepository where TEntity: IEntity
    {
     Task InsertAsync(TEntity entity);
     Task UpdateAsync(TEntity entity);
     Task DeleteAsync(TEntity entity);    
    }
}