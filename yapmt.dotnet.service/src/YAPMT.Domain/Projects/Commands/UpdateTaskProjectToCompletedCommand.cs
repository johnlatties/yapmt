using System;
using YAPMT.Core.Domain.Commands;
using YAPMT.Domain.Enums;

namespace YAPMT.Domain.Projects.Commands
{
    public class UpdateStatusTaskProjectCommand : ICommand
    {
        public Guid ProjectId { get; set; }

        public Guid TaskId { get; set; }

        public TaskStatus Status { get; set; }
                
    }
}