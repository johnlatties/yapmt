import { Component, OnInit } from '@angular/core';
import { MessageBusService } from '../../services/message-bus.service';
import { CREATE_PROJECT } from '../../services/actionsType';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
    public messageBusService: MessageBusService
  ) { }

  ngOnInit() {
  }

  onCreateProject() {
    this.messageBusService.publish(CREATE_PROJECT, true);
  }

}
