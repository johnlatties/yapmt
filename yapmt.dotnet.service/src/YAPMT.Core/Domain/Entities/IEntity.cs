using System;

namespace YAPMT.Core.Domain.Entities
{
    public interface IEntity
    {
        Guid Id { get; }
    }
}