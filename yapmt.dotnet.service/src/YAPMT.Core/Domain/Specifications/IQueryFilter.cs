using System;
using System.Linq.Expressions;
using YAPMT.Core.Domain.Entities;

namespace YAPMT.Core.Domain.Specifications
{
    public interface IQueryFilter<TEntity> where TEntity : class, IEntity
    {
         Expression<Func<TEntity, bool>> SatisfiedBy();
         bool IsSatisfiedBy(TEntity entity);
    }
}