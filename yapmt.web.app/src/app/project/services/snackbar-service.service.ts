import { Injectable } from '@angular/core';

@Injectable()
export class SnackbarServiceService {

  constructor() { }

  bootstrap() {
    document.body.insertAdjacentHTML('afterbegin', '<div id="snackbar-element"></div>');
  }

  showSuccess(message, time?) {
    this.showSnackbar(true, message, time);
  }

  showSuccessDefault() {
    this.showSnackbar(true, 'Saved!');
  }

  showWarning(message, time?) {
    this.showSnackbar(false, message, time);
  }

  showWarningDefault() {
    this.showSnackbar(false, 'We had a problem. Sorry :(', null);
  }

  private showSnackbar(success, message, time?) {
    const snackbar = document.getElementById('snackbar-element');
    const classShow = success ? 'show success-snackbar' : 'show warning-snackbar';
    snackbar.className = classShow;

    snackbar.insertAdjacentText('afterbegin', message);

    setTimeout(() => {
      snackbar.className = snackbar.className.replace(classShow, '');
      snackbar.innerText = '';
    }, time || 3000);
  }

}
