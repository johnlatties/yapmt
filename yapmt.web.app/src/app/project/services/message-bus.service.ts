import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';

export interface IMessage {
    messageType: string;
    data: any;
}

@Injectable()
export class MessageBusService {
    private message$: Subject<IMessage>;

    constructor() {
        this.message$ = new Subject<IMessage>();
    }

    public publish(messageType, message): void {
        this.message$.next({ messageType, data: message });
    }

    public of(messageType): Observable<any> {
        return this.message$.filter((m) => m.messageType === messageType).map((m) => m.data);
    }
}
