using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace YAPMT.Core.Domain.Handlers
{
    public sealed class NotificationEntry
    {
        private readonly List<string> messagesList = new List<string>();

        internal NotificationEntry()
        {
        }

        [JsonConstructor]
        internal NotificationEntry(List<string> messagesList)
        {
            if(messagesList == null)
                throw new ArgumentNullException(nameof(messagesList));
            
            messagesList.AddRange(messagesList);
        }

        public IReadOnlyList<string> Messages => messagesList;

        internal void AddMessage(string message){
            if(message == null)
                throw new ArgumentNullException(nameof(message));

            if(!messagesList.Exists(match => match.Equals(message, StringComparison.InvariantCultureIgnoreCase)))
                messagesList.Add(message);
        }

    }
}