
import { Headers, Http, Response, RequestOptions, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { API_ROOT } from '../../app.config';
import { Injectable } from '@angular/core';


@Injectable()
export class ProjectServer {

    constructor(
        private http: Http,
    ) {

    }

    getProjects(): Observable<any> {
        return this.http.get(`${API_ROOT}/Projects/`, this.getOptionsHeader())
            .map((res) => res.json());
    }


    getProject(projectId): Observable<any> {
        return this.http.get(`${API_ROOT}/Projects/${projectId}/Tasks`, this.getOptionsHeader())
            .map((res) => res.json());
    }


    createProject(data): Observable<any> {
        return this.http.post(`${API_ROOT}/Projects/`, data, this.getOptionsHeader())
            .map((res) => res.json());
    }

    deleteProject(projectId): Observable<any> {
        return this.http.delete(`${API_ROOT}/Projects/${projectId}`)
            .map((res) => res.json());
    }


    createTask(projectId, data): Observable<any> {
        return this.http.post(`${API_ROOT}/Projects/${projectId}/Tasks`, data, this.getOptionsHeader())
            .map((res) => res.json());
    }

    updateTask(projectId, taskId, status): Observable<any> {
        return this.http.patch(`${API_ROOT}/Projects/${projectId}/Tasks/${taskId}/Status`, status, this.getOptionsHeader())
            .map((res) => res.json());

    }

    getOptionsHeader(): RequestOptions {
        return new RequestOptions({ headers: this.getHeaders() });
    }

    getHeaders(): Headers {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return headers;
    }

    getHeaderAndOptions(): RequestOptions {
        const opt = new RequestOptions({ headers: this.getHeaders() });
        return opt;
    }

    getFileOptionsHeader(): RequestOptions {
        return new RequestOptions({ headers: this.getHeaders(), responseType: ResponseContentType.Blob });
    }

}
