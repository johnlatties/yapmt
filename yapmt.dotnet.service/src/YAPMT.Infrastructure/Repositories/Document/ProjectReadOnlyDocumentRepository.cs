using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using YAPMT.Core.Domain.Specifications;
using YAPMT.Core.Infrastructure.Repositories.Document;
using YAPMT.Domain.Enums;
using YAPMT.Domain.Projects.Entities;
using YAPMT.Domain.Projects.Filter;
using YAPMT.Domain.Projects.Repositories;

namespace YAPMT.Infrastructure.Repositories.Document
{
    public class ProjectReadOnlyDocumentRepository : ReadOnlyDocumentRepository<Project>, IProjectReadOnlyDocumentRepository
    {
        public ProjectReadOnlyDocumentRepository(IMongoClient mongoClient, MongoUrl mongoUrl)
         : base(mongoClient, mongoUrl)
        {
        }

        public async Task<Project> GetTaskProject(IQueryFilter<Project> spec)
        {
            
            var project = await CheckLateTask(spec);
            return project;
        }

        private async Task<Project> CheckLateTask(IQueryFilter<Project> spec)
        {
            var project = await this.GetAsync(spec);
            if(project is null) return null;
            
            project
            .Tasks
            .Where(t => t.DueDate < DateTime.Now && t.Status == Domain.Enums.TaskStatus.InTime)
            .ToList()
            .ForEach( task => { task.SetAsLate(); });

            await Collection.ReplaceOneAsync(spec.SatisfiedBy(), project);
            project.Tasks.OrderBy(t => t.DueDate);
            return project;
        }
    }
}