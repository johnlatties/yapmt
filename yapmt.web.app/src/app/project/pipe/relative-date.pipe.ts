import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'relativeDate',
    pure: false
})
export class RelativeDatePipe implements PipeTransform {
    private timer: number;

    transform(date) {
        const informedDate = new Date(date);
        const now = new Date();
        const description = now > informedDate ? 'ago' : 'to expire';
        const seconds = Math.round(Math.abs((now.getTime() - informedDate.getTime()) / 1000));
        const minutes = Math.round(Math.abs(seconds / 60));
        const hours = Math.round(Math.abs(minutes / 60));
        const days = Math.round(Math.abs(hours / 24));
        const months = Math.round(Math.abs(days / 30.416));
        const years = Math.round(Math.abs(days / 365));
        if (seconds <= 45) {
            return `a few seconds ${description}`;
        } else if (seconds <= 90) {
            return `a minute ${description}`;
        } else if (minutes <= 45) {
            return `${minutes} minutes ${description}`;
        } else if (minutes <= 90) {
            return 'an hour ago';
        } else if (hours <= 23) {
            return `${hours} hours ${description}`;
        } else if (hours <= 36) {
            return 'a day ago';
        } else if (days <= 25) {
            return `${days} days ${description}`;
        } else if (days <= 45) {
            return 'a month ago';
        } else if (days <= 345) {
            return `${months} months ${description}`;
        } else if (days <= 545) {
            return `a year ${description}`;
        } else {
            return `${years} years ${description}`;
        }

    }

}
