import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SnackbarServiceService } from '../../services/snackbar-service.service';
import { ProjectServer } from '../../services/project.server';
import { MessageBusService } from '../../services/message-bus.service';
import { GET_PROJECT } from '../../services/actionsType';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.css']
})
export class TaskDetailComponent implements OnInit {

  editing;
  formTask: FormGroup;
  public taskDescription: string;
  public taskOwner: string;
  public taskDueDate: string;
  @Input() projectId;
  constructor(
    public snackbar: SnackbarServiceService,
    private projectServer: ProjectServer,
    public messageBusService: MessageBusService
  ) { }

  ngOnInit() {
    this.formTask = new FormGroup({
      taskDescription: new FormControl('', [
        Validators.required, Validators.minLength(30)
      ]),
      taskOwner: new FormControl('', [
        Validators.required, Validators.minLength(30)
      ]),
      taskDueDate: new FormControl('', [
        Validators.required, Validators.minLength(5)
      ]),
    });
  }

  onSave(e) {
    if (e.keyCode === 13) {
      e.preventDefault();
      if (!this.taskDescription || !this.taskOwner || !this.taskDueDate) { return; }
      const dueDateValid = this.validateDueDate(this.taskDueDate);
      if (!dueDateValid) {
        this.snackbar.showWarning('Invalid due date');
        return;
      }
      this.projectServer.createTask(this.projectId, {
        projectId: this.projectId,
        description: this.taskDescription,
        owner: this.taskOwner,
        dueDate: this.getDate(this.taskDueDate)
      }).subscribe((data) => {
        this.snackbar.showSuccessDefault();
        this.messageBusService.publish(GET_PROJECT, this.projectId);
      }, (erro) => {
        console.log(erro);
        this.snackbar.showWarningDefault();
      });
    }
  }

  getDate(taskDueDate) {
    const month = taskDueDate.split('/')[0];
    const day = taskDueDate.split('/')[1];
    return new Date(new Date().getFullYear(), month - 1, day);
  }

  validateDueDate(taskDueDate) {
    if ((taskDueDate.length !== 5)
      || (taskDueDate.indexOf('/') === -1)
      || (taskDueDate.substring(2, 3) !== '/')
    ) {
      return false;
    }
    const month = taskDueDate.split('/')[0];
    const day = taskDueDate.split('/')[1];

    const monthTrue = parseInt(month);
    const dayTrue = parseInt(day);

    if (!(monthTrue == month) || (monthTrue <= 0 || monthTrue > 12)
      || !(dayTrue == day) || (dayTrue > 31 || dayTrue <= 0) || (monthTrue === 2 && dayTrue > 28)) {
      return false;
    }

    return true;

  }

  setEditingState() {
    this.editing = !this.editing;
    this.taskDescription = '';
    this.taskOwner = '';
    this.taskDueDate = '';
  }

}
