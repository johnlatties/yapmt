using System;
using YAPMT.Core.Domain.Commands;

namespace YAPMT.Domain.Projects.Commands
{
    public class DeleteProjectCommand : ICommand
    {
       public Guid Id { get; set; }
    }
}