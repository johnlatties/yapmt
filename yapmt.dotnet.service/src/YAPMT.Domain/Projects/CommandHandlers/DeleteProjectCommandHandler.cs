using System.Threading;
using System.Threading.Tasks;
using YAPMT.Core.Domain.Handlers;
using YAPMT.Domain.Projects.Commands;
using YAPMT.Domain.Projects.Filter;
using YAPMT.Domain.Projects.Repositories;

namespace YAPMT.Domain.Projects.CommandHandlers
{
    public class DeleteProjectCommandHandler : IHandlerAsync<DeleteProjectCommand>
    {
        private readonly IProjectWriteOnlyDocumentRepository ProjectWriteRepositoty;
        private readonly IProjectReadOnlyDocumentRepository ProjectReadRepositoty;
        public DeleteProjectCommandHandler(
            IProjectWriteOnlyDocumentRepository projectWriteRepositoty,
            IProjectReadOnlyDocumentRepository projectReadRepositoty)
        {
            ProjectWriteRepositoty = projectWriteRepositoty;
            ProjectReadRepositoty = projectReadRepositoty;
        }
        public async Task<Notification> Handle(DeleteProjectCommand request, CancellationToken cancellationToken)
        {
            var project = await ProjectReadRepositoty.GetAsync(ProjectFilter.ById(request.Id));

            if(project is null)
            {
                var notification = new Notification();
                notification.AddValidation("PojectNotDeleted", $"Could not find the project : {request.Id}");
                return notification;
            } 
            else
            {
                await ProjectWriteRepositoty.DeleteAsync(project);
                return new Notification(project);
            }
        }
    }
}