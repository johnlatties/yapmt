using MongoDB.Driver;

namespace YAPMT.Core.Infrastructure.Repositories.Document
{
    public interface IMongoDriveClientAdapter
    {
         IMongoClient MongoClient { get; }
    }
}