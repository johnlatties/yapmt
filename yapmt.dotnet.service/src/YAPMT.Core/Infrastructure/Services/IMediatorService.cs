using System.Threading.Tasks;
using YAPMT.Core.Domain.Commands;
using YAPMT.Core.Domain.Handlers;

namespace YAPMT.Core.Infrastructure.Services
{
    public interface IMediatorService : IService
    {
        Task<Notification> SendCommandAsync(ICommand command);

        Task RaiseEventAsync(INotificationHandler @event);
         
    }
}