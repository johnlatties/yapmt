using System;
using System.Linq.Expressions;
using YAPMT.Core.Domain.Entities;

namespace YAPMT.Core.Domain.Specifications
{
    public abstract class QueryFilter<TEntity> : IQueryFilter<TEntity> where TEntity : class, IEntity
    {
        public virtual bool IsSatisfiedBy (TEntity entity) => SatisfiedBy().Compile().Invoke(entity);
        public abstract Expression<Func<TEntity, bool>> SatisfiedBy();
    }
}