using System;
using MongoDB.Bson.Serialization.Attributes;

namespace YAPMT.Core.Domain.Entities
{
    public abstract class Entity : IEntity
    {
        [BsonId]
       public Guid Id { get; protected set; }

       public Entity()
       {
           Id = Guid.NewGuid();
       }
    }
}