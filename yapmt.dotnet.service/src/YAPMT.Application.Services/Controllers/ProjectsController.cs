﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using YAPMT.Core.Domain.Repositories;
using YAPMT.Core.Infrastructure.Services;
using YAPMT.Domain.Projects.Commands;
using YAPMT.Domain.Projects.Entities;
using YAPMT.Domain.Projects.Filter;
using AutoMapper;
using YAPMT.Application.Services.Mapper.Dtos.Project;
using System.Collections.Generic;
using YAPMT.Domain.Projects.Repositories;

namespace YAPMT.Application.Services.Controllers
{
    [Route("api/[controller]")]
    public class ProjectsController : Controller
    {
        private readonly IProjectReadOnlyDocumentRepository ProjectReadRepository;
        private readonly IMediatorService MediatorService;
        private readonly IMapper Mapper;

        public ProjectsController(
            IMediatorService mediatorService,
            IProjectReadOnlyDocumentRepository projectReadRepository,
            IMapper mapper
            )
        {
            MediatorService = mediatorService;
            ProjectReadRepository = projectReadRepository;
            Mapper = mapper;
        }


        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var projects = await ProjectReadRepository.QueryAllAsync();
            var result = Mapper.Map<ICollection<ProjectDto>>(projects);
            return Ok(result);
        }

        [HttpGet, Route("{id}")]
        public async Task<IActionResult> GetBy(Guid id)
        {
            var project = await ProjectReadRepository.GetAsync(ProjectFilter.ById(id));
            var result = Mapper.Map<ProjectDto>(project);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CreateProjectCommand command)
        {
            var notification = await MediatorService.SendCommandAsync(command);

            return Ok(notification);
        }

        [HttpDelete, Route("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var command = new DeleteProjectCommand { Id = id };
            var notification = await MediatorService.SendCommandAsync(command);

            return Ok(notification);
        }


        [HttpPost, Route("{id}/Tasks")]
        public async Task<IActionResult> CreateTask(Guid id, [FromBody]CreateTaskProjectCommand command)
        {
            command.ProjectId = id;
            var notification = await MediatorService.SendCommandAsync(command);

            return Ok(notification);
        }

        [HttpGet, Route("{id}/Tasks")]
        public async Task<IActionResult> GetTask(Guid id)
        {
            var project = await ProjectReadRepository.GetTaskProject(ProjectFilter.ById(id));
            var result = Mapper.Map<ProjectWithTaksDto>(project);
            return Ok(result);
        }


        [HttpPatch, Route("{id}/Tasks/{taskId}/Status")]
        public async Task<IActionResult> CompleteTask(Guid id, Guid taskId, [FromBody]Domain.Enums.TaskStatus status)
        {
            var command = new UpdateStatusTaskProjectCommand {ProjectId = id, TaskId = taskId, Status = status};
            var notification = await MediatorService.SendCommandAsync(command);

            return Ok(notification);
        }
    }
}
