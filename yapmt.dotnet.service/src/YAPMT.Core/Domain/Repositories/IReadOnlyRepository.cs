using System.Collections.Generic;
using System.Threading.Tasks;
using YAPMT.Core.Domain.Entities;
using YAPMT.Core.Domain.Specifications;
using YAPMT.Core.Infrastructure.Repositories;

namespace YAPMT.Core.Domain.Repositories
{
    public interface IReadOnlyRepository<TEntity> : IRepository where TEntity :  class, IEntity 
    {
        Task<TEntity> GetAsync(IQueryFilter<TEntity> spec);
        Task<List<TEntity>> QueryAsync(IQueryFilter<TEntity> spec);
        Task<long> CountAsync(IQueryFilter<TEntity> spec);
        Task<bool> ExistsAsync(IQueryFilter<TEntity> spec);
        Task<List<TEntity>> QueryAllAsync(int limitCount = RepositoryConstants.DefaultQueryLimitCount);
    }
}