namespace YAPMT.Domain.Enums
{
    public enum TaskStatus
    {
        InTime = 1,
        Late = 2,
        Completed = 3,
    }
}