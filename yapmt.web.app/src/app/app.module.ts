import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ProjectComponent } from './project/project.component';
import { HeaderComponent } from './project/components/header/header.component';
import { ProjectListComponent } from './project/components/project-list/project-list.component';
import { TaskListComponent } from './project/components/task-list/task-list.component';
import { ProjectViewComponent } from './project/components/project-view/project-view.component';
import { ProjectDetailComponent } from './project/components/project-detail/project-detail.component';
import { ProjectServer } from './project/services/project.server';
import { MessageBusService } from './project/services/message-bus.service';
import { routing } from './app.routing';
import { HttpModule } from '@angular/http';
import { SnackbarServiceService } from './project/services/snackbar-service.service';
import { TaskDetailComponent } from './project/components/task-detail/task-detail.component';
import { AddAtSignalPipe } from './project/pipe/add-at-signal.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ProjectComponent,
    HeaderComponent,
    ProjectListComponent,
    TaskListComponent,
    ProjectViewComponent,
    ProjectDetailComponent,
    TaskDetailComponent,
  ],
  imports: [
    BrowserModule,
    routing,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    ProjectServer,
    MessageBusService,
    SnackbarServiceService
  ],
  entryComponents: [
    ProjectListComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
