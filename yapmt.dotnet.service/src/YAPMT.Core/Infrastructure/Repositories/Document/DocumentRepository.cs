using System;
using MongoDB.Bson;
using MongoDB.Driver;
using YAPMT.Core.Domain.Entities;
using YAPMT.Core.Domain.Specifications;

namespace YAPMT.Core.Infrastructure.Repositories.Document
{
    public abstract class DocumentRepository<TEntity> : IMongoDriveClientAdapter where TEntity : class, IEntity
    {
        protected readonly IMongoClient MongoClient;
        protected readonly IMongoDatabase MongoDatabase;
        protected readonly Lazy<IMongoCollection<TEntity>> MongoCollection;

        public DocumentRepository(IMongoClient mongoClient, MongoUrl mongoUrl)
        {
            MongoClient = mongoClient;
            MongoDatabase = mongoClient.GetDatabase(mongoUrl?.DatabaseName ?? throw new ArgumentException(nameof(mongoUrl)));
            MongoCollection = new Lazy<IMongoCollection<TEntity>>(() => GetCollection());
        }
        IMongoClient IMongoDriveClientAdapter.MongoClient => MongoClient;
        protected IMongoCollection<TEntity> Collection => MongoCollection.Value;
        protected IMongoDatabase Database => MongoDatabase;
        protected void ValidateQueryArgument(IQueryFilter<TEntity> spec) { if(spec == null) throw new ArgumentNullException(nameof(spec));}
        protected void ValidateEntityArgument(Object entity) { if(entity == null) throw new ArgumentException(nameof(entity)); }
        protected IMongoCollection<TEntity> GetCollection() => MongoDatabase.GetCollection<TEntity>(typeof(TEntity).Name);

    }
}