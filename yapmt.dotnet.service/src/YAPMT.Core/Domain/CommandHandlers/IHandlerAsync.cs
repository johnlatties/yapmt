using MediatR;
using YAPMT.Core.Domain.Commands;

namespace YAPMT.Core.Domain.Handlers
{
    public interface IHandlerAsync<in TCommand> : IRequestHandler<TCommand, Notification> where TCommand : ICommand
    {
    }
}