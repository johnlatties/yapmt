using Xunit;
using YAPMT.Domain.Projects.Entities;

namespace YAPMT.UnitTests
{
    public class ProjectUnitTest {

        [Fact]
        public void ProjectShouldHaveAName ()
        {
            var name = "New Project";
            var newProject = new Project (name);

            Assert.Equal (name, newProject.Name);
        }

        [Fact]
        public void ProjectShouldAllowAddingTasks ()
        {

        }
    }
}