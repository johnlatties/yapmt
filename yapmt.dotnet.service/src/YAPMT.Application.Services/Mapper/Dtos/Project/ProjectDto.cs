using System;

namespace YAPMT.Application.Services.Mapper.Dtos.Project
{
    public class ProjectDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}