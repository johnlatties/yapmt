using System;
using YAPMT.Core.Domain.Entities;
using YAPMT.Domain.Enums;

namespace YAPMT.Domain.Projects.Entities
{
    public class TaskProject : Entity
    {
        protected TaskProject() { }
        public TaskProject(Guid projectId, string description, string owner, DateTime dueDate)
        {
            ProjectId = projectId;
            Description = description;
            Owner = owner;
            DueDate = dueDate;
            Status = dueDate > DateTime.Now ? TaskStatus.InTime : TaskStatus.Late;
        }

        public Guid ProjectId { get; private set; }
        public string Description { get; private set; }
        public string Owner { get; private set; }
        public DateTime DueDate { get; private set; }
        public TaskStatus Status { get; private set; }

        public void SetStatus(TaskStatus newStatus)
        {
            Status = newStatus;
        }
        public void SetAsInTime() => Status = TaskStatus.InTime;
        public void SetAsLate() => Status = TaskStatus.Late;
    }
}