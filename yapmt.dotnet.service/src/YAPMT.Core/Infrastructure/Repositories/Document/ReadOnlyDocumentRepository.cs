using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using YAPMT.Core.Domain;
using YAPMT.Core.Domain.Entities;
using YAPMT.Core.Domain.Repositories;
using YAPMT.Core.Domain.Specifications;
using MongoDB.Bson;

namespace YAPMT.Core.Infrastructure.Repositories.Document
{
    public class ReadOnlyDocumentRepository<TEntity> :  DocumentRepository<TEntity>, IReadOnlyRepository<TEntity>
     where TEntity : class, IEntity
    {
        public ReadOnlyDocumentRepository(IMongoClient mongoClient, MongoUrl mongoUrl)
        : base(mongoClient, mongoUrl)
        {}
        public Task<long> CountAsync(IQueryFilter<TEntity> spec)
        {
            return FilterCollection(spec).CountAsync();
        }

        public Task<bool> ExistsAsync(IQueryFilter<TEntity> spec)
        {
            return FilterCollection(spec).AnyAsync();
        }
        public async Task<TEntity> GetAsync(IQueryFilter<TEntity> spec)
        {
            return await FilterCollection(spec).FirstOrDefaultAsync();
        }

        public Task<List<TEntity>> QueryAllAsync(int limitCount = 100)
        {
            return Collection
                .Find(new BsonDocument())
                .Limit(limitCount)
                .ToListAsync();
        }

        public Task<List<TEntity>> QueryAsync(IQueryFilter<TEntity> spec)
        {
           return FilterCollection(spec).ToListAsync();
        }

        protected IFindFluent<TEntity, TEntity> FilterCollection(IQueryFilter<TEntity> spec){
            ValidateQueryArgument(spec);
            return Collection.Find(spec.SatisfiedBy());
        }
    }
}