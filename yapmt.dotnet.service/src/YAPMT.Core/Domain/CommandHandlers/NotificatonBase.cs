using System;
using System.Collections.Generic;
using System.Linq;

namespace YAPMT.Core.Domain.Handlers {
    public class NotificationBase<TData> : INotificationHandler
    {
        private readonly Dictionary<string, NotificationEntry> errors = new Dictionary<string, NotificationEntry> ();
        private readonly Dictionary<string, NotificationEntry> warnings = new Dictionary<string, NotificationEntry> ();
        private readonly Dictionary<string, NotificationEntry> validations = new Dictionary<string, NotificationEntry> ();

        public NotificationBase () { }
        public NotificationBase (TData outputData) {
            if (outputData == null)
                throw new ArgumentNullException(nameof(outputData));
            this.OutputData = outputData;
        }
        public TData OutputData { get; private set; }

        public bool HasMessages => errors.Any () || warnings.Any () || validations.Any ();
        public IReadOnlyDictionary<string, NotificationEntry> Erros => errors;
        public IReadOnlyDictionary<string, NotificationEntry> Warnings => warnings;
        public IReadOnlyDictionary<string, NotificationEntry> Validations => validations;

        public bool HasOutputData => OutputData != null;

        public void AddError(string key, string message)
        {
            AddMessage(key, message, errors);
        }

        public void AddWarning(string key, string message)
        {
            AddMessage(key, message, warnings);
        }

        public void AddValidation(string key, string message)
        {
            AddMessage(key, message, validations);
        }

        public virtual TData GetOutputData()
        {
            if (OutputData == null)
                return default(TData);

            return OutputData;
        }

        private static void AddMessage(string key, string message, Dictionary<string, NotificationEntry> entries)
        {
            if (key == null)
                throw new ArgumentNullException(nameof(key));

            if (message == null)
                throw new ArgumentNullException(nameof(message));

            if (!entries.TryGetValue(key, out NotificationEntry entry))
            {
                entry = new NotificationEntry();
                entries.Add(key, entry);
            }

            entry.AddMessage(message);
        }

    }
}